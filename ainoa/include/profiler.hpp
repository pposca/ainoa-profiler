#ifndef AINOA_PROFILER_HPP
#define AINOA_PROFILER_HPP

#include <chrono>
#include <unordered_map>

using namespace std;

namespace Ainoa {

    class Profiler {        

        public:
            using clock = chrono::high_resolution_clock;

            Profiler(const string mark_name);
            int get_size();
            bool is_empty();
            void set_mark(const string mark_name);
            clock::time_point get_mark(const string mark_name);

            template <class T> long time_from(const string mark_name);
                        
        private:
            unordered_map<string, clock::time_point> marks;

    };

    template <class T>
    long Profiler::time_from(const string mark_name)
    {
        return(chrono::duration_cast<T>(clock::now() - marks[mark_name]).count());
    }

}

#endif