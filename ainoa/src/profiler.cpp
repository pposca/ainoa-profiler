#include "profiler.hpp"
#include <iostream>

namespace Ainoa 
{
    Profiler::Profiler(const string mark_name) 
    {
        set_mark(mark_name);
    }

    int Profiler::get_size() 
    {
        return(marks.size());
    }

    bool Profiler::is_empty() 
    {
        return(marks.empty());
    }

    void Profiler::set_mark(const string mark_name) 
    {
        marks[mark_name] = Profiler::clock::now();
    }

    Profiler::clock::time_point Profiler::get_mark(const string mark_name) 
    {
        return(marks.at(mark_name));
    }

}