# Ainoa Profiler

Ainoa Profiler is a C++11 class to **benchmark execution time** between 2 points inside a program.

Its main features are:

	- Simple to use and understand.
	- Small size.
	- Well documented.
	- No dependencies, except for C++11 standard library.

Have fun!

:)